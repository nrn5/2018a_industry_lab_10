package ictgradschool.industry.lab10;

public class ex03 {


    private static double bar ( double x , int n ) {

        if (n > 1)
            return x * bar(x, n - 1);
        else if (n < 0)
            return 1.0 / bar(x, -n);
        else
            return x;
    }

    public static void main(String[] args) {
        System.out.println(ex03.bar(3, -2));
    }
}
